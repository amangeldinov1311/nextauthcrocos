import { DefaultSession, DefaultUser } from "next-auth";
// Define a role enum
export enum RoleEnum {
  user = "user",
  admin = "admin",
}

//common interface for JWT and Session
interface IUser extends DefaultUser {
  id: number;
  role_id: number;
  first_name: string;
  last_name: string;
  middle_name: any;
  email: string;
  email_verified_at: string;
  status: string;
  activation_at: any;
  deactivation_at: any;
  created_at: string;
  updated_at: string;
  deleted_at: any;
  full_name: string;
  role: Rolee;
}
export interface Role {
  id: number;
  slug: RoleEnum;
  name: string;
  created_at: string;
  updated_at: string;
  deleted_at: any;
}
declare module "next-auth" {
  interface User extends IUser {}
  interface Session {
    user?: User;
  }
}
declare module "next-auth/jwt" {
  interface JWT extends IUser {}
}
