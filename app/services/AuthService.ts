import { ILogin } from "../constants/interfaces";
import http from "../utils/http";

const signInUser = (data: ILogin) => {
  return http.post("login", data);
};

export const AuthService = {
  signInUser,
};
