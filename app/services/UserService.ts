import { User } from "next-auth";
import { IUserService } from "../constants/interfaces";
import { AuthService } from "./AuthService";

export class InMemoryUserService implements IUserService {
  async signInCredentials(email: string, password: string): Promise<User> {
    const res = await AuthService.signInUser({
      email: email,
      password: password,
    });
    const user = res.data.data as User;

    if (!user) {
      throw new Error("Invalid email or password");
    }

    return user;
  }
}

export const userService = new InMemoryUserService();
