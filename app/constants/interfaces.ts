import { User } from "next-auth";

export interface IUserService {
  signInCredentials(email: string, password: string): Promise<User> | User;
}
export interface ILogin {
  email: string;
  password: string;
}
