import Link from "next/link";
import React from "react";
import { signOut, useSession } from "next-auth/react";

const AdminPage = () => {
  const { data: session } = useSession();
  console.log(session);
  const user = session?.user;
  return (
    <div className="container max-w-[100%] ">
      <div className="grid place-content-center min-h-screen">
        <div className="flex flex-col gap-4">
          <h1 className="text-4xl">Admin Page</h1>

          <Link className="btn" href="/">
            Go to Index Page
          </Link>
          <button className="btn btn-outline" onClick={() => signOut()}>
            Sign Out
          </button>
        </div>
      </div>
    </div>
  );
};

export default AdminPage;
